from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base, db
from app.utils import *
from app.models.AccountUser import AccountUser
from app.models.Group import Group
from app.models.Tag import Tag

class Ladi(Base):
    __tablename__ = "ladies"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    group_id = Column(Integer, ForeignKey('groups.id'))
    remote_id = Column(String)
    alias = Column(String)
    domain = Column(String)
    path = Column(String)
    https = Column(Integer)
    page_url = Column(String)
    last_update_source = Column(TIMESTAMP)
    remote_updated_at = Column(TIMESTAMP)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    group = relationship('Group', uselist=False)
    items = relationship('Item')
    tags = relationship('Tag', secondary='ladi_tag')

    def load_data_from_remote_json(self, data):
        self.name = data['name']
        self.remote_id = data['_id']
        self.alias = data['alias']
        self.domain = data['domain']
        self.path = data['path']
        self.https = data['https']
        self.page_url = data['page_url']
        self.last_update_source = parser.parse(data['last_update_source'])
        self.remote_updated_at = parser.parse(data['updated_at'])
        self.created_at = parser.parse(data['created_at'])
        self.updated_at = datetime.datetime.now()
        return self

    def keys_of_item(self, data):
        keys = {
            'tags': data['tags'],
            'name': data['name'],
            'alias': data['alias'],
            'domain': data['domain'],
            'path': data['path'],
            'page_url': data['page_url']
        }
        return keys

    def match_checksum(self, keys):
        try:
            checksum = create_check_sum(keys)
            if get_cache_checksum(self.__table__, self.remote_id, checksum):
                logging.info('match checksum ladi')
                return True
        except Exception as e:
            logging.exception(e)
        return False

    def save_checksum(self, keys):
        try:
            checksum = create_check_sum(keys)
            set_cache_checksum(self.__table__, self.remote_id, checksum)
            tags = keys['tags']
            del(keys['tags'])
            set_item_from_keys(self, keys)
            self.tags = []
            for tag_id in tags:
                tag = Tag.query.filter_by(remote_id=tag_id).first()
                if not tag:
                    tag = Tag()
                    tag.remote_id = tag_id
                    db.add(tag)
                self.tags.append(tag)
            db.commit()
            logging.info('save checksum ladi')
            return True
        except Exception as e:
            logging.exception(e)
        return False

    def create(self):
        db.add(self)
        db.commit()
        db.close()