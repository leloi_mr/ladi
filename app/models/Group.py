from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base

# @dataclass
class Group(Base):
    __tablename__ = "groups"
    TYPE_NEW = 1
    TYPE_NORMAL = 2
    TYPE_REMOVED = 3
    id = Column(Integer, primary_key=True)
    account_user_id = Column(Integer, ForeignKey('account_user.id'))
    name = Column(String)
    description = Column(String)
    type = Column(Integer)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    account_user = relationship('AccountUser', uselist=False)
    ladies = relationship('Ladi')