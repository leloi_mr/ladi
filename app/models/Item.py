from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base

class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True)
    ladi_id = Column(Integer, ForeignKey('ladies.id'))
    status = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    ladies = relationship('Ladi')