from sqlalchemy import Column
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base, db
from app.utils import *

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    uid = Column(String)
    name = Column(String)
    email = Column(String)
    email_verified_at = Column(TIMESTAMP)
    password = Column(String)
    remember_token = Column(String)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)

    accounts = relationship('Account', secondary='account_user')

    def check_has_pasting(self, remote_ladi_source_dumps):
        try:
            script_url = self.uid +'.js'
            if script_url in remote_ladi_source_dumps:
                return True
        except Exception as e:
            logging.exception(e)
        return False

    def get_new_source(self, has_pasted, remote_ladi_source_dumps):
        try:
            script_url = self.uid +'.js'
            script_html = self.uid +'.js'
            if has_pasted:
                # replace
                return True
            # add new
            return script_html
        except Exception as e:
            logging.exception(e)
        return None

    def add_account(self, account):
        self.accounts.append(account)
        db.commit()
        db.close()
