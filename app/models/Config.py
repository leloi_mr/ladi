from sqlalchemy import Column
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship
from app.database import Base, db

class Config(Base):
    __tablename__ = "configs"

    id = Column(Integer, primary_key=True)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    login_api_url = Column(String)
    my_info_api_url = Column(String)
    list_all_ladi_api_url = Column(String)
    detail_ladi_api_url = Column(String)
    update_ladi_api_url = Column(String)
    publish_ladi_url = Column(String)