from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base
from app.models import Account, User

class AccountUser(Base):
    __tablename__ = "account_user"

    id = Column(Integer, primary_key=True)
    account_id = Column(Integer, ForeignKey('accounts.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    type = Column(Integer)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    account = relationship('Account', backref=backref("account_user", cascade="all, delete-orphan"))
    user = relationship('User', backref=backref("account_user", cascade="all, delete-orphan"))
    groups = relationship('Group')