from sqlalchemy import Column
from sqlalchemy.types import Integer, String, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base, db
import requests
from app.utils import *
from app.models.Ladi import Ladi

class Account(Base):
    __tablename__ = "accounts"

    id = Column(Integer, primary_key=True)
    email = Column(String)
    name = Column(String)
    alias = Column(String)
    password = Column(String)
    token = Column(String)
    remote_id = Column(String)
    status = Column(Integer, default=1)
    status_scan = Column(Integer, default=1)
    status_paste = Column(Integer, default=1)
    status_block = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    users = relationship('User', secondary='account_user')

    def is_login(self):
        try:
            count_retry = 1
            while count_retry > 0:
                if self.token:
                    cache_updated_at = get_cache_updated_at(self.__table__, self.id)
                    if cache_updated_at and datetime.datetime.now() - cache_updated_at < datetime.timedelta(minutes=30):
                        logging.info('match updated_at account')
                        return True
                    if self.check_info():
                        return True
                if self.check_login():
                    if self.check_info():
                        return True
                count_retry -= 1
                time.sleep(5)
        except Exception as e:
            logging.exception(e)
        return False

    def check_info(self):
        try:
            logging.info('check info')
            res = self.send_post(conf.my_info_api_url)
            if res and res.status_code == 200:
                data = json.loads(res.text)
                data = data['data']['store']
                keys = self.keys_of_item(data)
                if not self.match_checksum(keys):
                    self.save_checksum(keys)
                self.save_update_at()
                return True
        except Exception as e:
            logging.exception(e)
        return False

    def update_remote_ladi(self, remote_id, new_source):
        try:
            res = self.send_post(conf.update_ladi_api_url, {
                "id": remote_id,
                "lang": "vi",
                "source": new_source
            })
            if res and res.status_code == 200:
                return True
        except Exception as e:
            logging.exception(e)
        return False

    def keys_of_item(self, data):
        keys = {
            'remote_id': data['_id'],
            'name': data['name'],
            'alias': data['alias']
        }
        return keys

    def match_checksum(self, keys):
        try:
            checksum = create_check_sum(keys)
            if get_cache_checksum(self.__table__, self.id, checksum):
                logging.info('match checksum account')
                return True
        except Exception as e:
            logging.exception(e)
        return False

    def save_checksum(self, keys):
        try:
            checksum = create_check_sum(keys)
            set_cache_checksum(self.__table__, self.id, checksum)
            set_item_from_keys(self, keys)
            logging.info('save checksum account')
            db.commit()
            return True
        except Exception as e:
            logging.exception(e)
        return False

    def save_update_at(self, updated_at = datetime.datetime.now()):
        try:
            set_cache_updated_at(self.__table__, self.id, updated_at)
            set_item_updated_at(self, updated_at)
            logging.info('save update_at account')
            db.commit()
            return True
        except Exception as e:
            logging.exception(e)
        return False

    def check_login(self):
        try:
            res = requests.post(
                conf.login_api_url,
                data={
                    'email': self.email,
                    'password': self.password,
                }
            )
            if res and res.status_code == 200:
                txt = res.text
                txt = txt and json.loads(txt) or None
                if txt and txt['data'] and txt['token']:
                    self.token = txt['data']['token']
                    return True
        except Exception as e:
            logging.exception(e)
        return False

    def get_all_remote_ladi(self):
        try:
            res = self.send_post(conf.list_all_ladi_api_url, {
                "is_delete": "false",
                "is_publish": "true",
                "keyword": "",
                "lang": "vi",
                "limit": 5,
                "page": 1
            })
            if res:
                data = json.loads(res.text)
                return data
        except Exception as e:
            logging.exception(e)
        return None

    def get_detail_remote_ladi(self, remote_id):
        try:
            res = self.send_post(conf.detail_ladi_api_url, {
                "id": remote_id,
                "lang": "vi"
            })
            if res:
                data = json.loads(res.text)
                return data
        except Exception as e:
            logging.exception(e)
        return None

    def send_get(self, url):
        if not self.token:
            return None
        try:
            timeout = 10
            headers = {
                'authorization': self.token,
                'content-type': "application/json;charset=UTF-8"
            }
            res = requests.get(
                url,
                timeout = timeout,
                headers = headers
            )
            if res.status_code == 200:
                return res
        except Exception as e:
            logging.exception(e)
        return None

    def send_post(self, url, data = {}):
        if not self.token:
            return None
        try:
            timeout = 10
            headers = {
                'authorization': self.token,
                'content-type': 'application/json;charset=UTF-8',
            }
            res = requests.post(
                url,
                data = json.dumps(data),
                timeout = timeout,
                headers = headers
            )
            if res.status_code == 200:
                return res
        except Exception as e:
            logging.exception(e)
        return None