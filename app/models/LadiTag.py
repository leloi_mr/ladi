from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from app.database import Base
from app.models import Account, User

class LadiTag(Base):
    __tablename__ = "ladi_tag"

    id = Column(Integer, primary_key=True)
    ladi_id = Column(Integer, ForeignKey('ladies.id'))
    tag_id = Column(Integer, ForeignKey('tags.id'))

    ladi = relationship('Ladi', backref=backref("ladi_tag", cascade="all, delete-orphan"), uselist=False)
    tag = relationship('Tag', backref=backref("ladi_tag", cascade="all, delete-orphan"), uselist=False)