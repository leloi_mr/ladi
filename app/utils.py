from flask.json import JSONEncoder
import logging, datetime, json, time, redis, zlib
from dateutil import tz, parser

conf = {
    'id': 2,
    'status': 1,
    'status_scan': 1,
    'status_paste': 2,
    'status_block': 1,
    'login_api_url': 'https://api.ladiuid.com/2.0/auth/signin',
    'my_info_api_url': 'https://api.ladipage.com/2.0/store/info',
    'list_all_ladi_api_url': 'https://api.ladipage.com/2.0/ladi-page/list',
    'detail_ladi_api_url': 'https://api.ladipage.com/2.0/ladi-page/show',
    'update_ladi_api_url': '',
    'publish_ladi_url': ''
}

log_filename = 'log.txt'
logging.basicConfig(
    level = logging.INFO,
    format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    handlers=[logging.FileHandler(log_filename, 'w', 'utf-8')]
)
default_tzinfo = tz.gettz("Asia/Ho_Chi_Minh")

CONNECT_REDIS_CACHE_UPDATED_AT = {
    'host': 'localhost',
    'port': 6379,
    'db': 2
}
CONNECT_REDIS_CACHE_CHECKSUM = {
    'host': 'localhost',
    'port': 6379,
    'db': 3
}

def set_item_created_at(item, value=datetime.datetime.now()):
    item.updated_at = value

def set_item_updated_at(item, value=datetime.datetime.now()):
    item.updated_at = value

def set_item_deleted_at(item, value=datetime.datetime.now()):
    item.updated_at = value

def parse_tz(res):
    return res.replace(tzinfo=res.tzinfo or default_tzinfo)

def get_cache_updated_at(key, id):
    try:
        pool_redis = redis.ConnectionPool(
            host=CONNECT_REDIS_CACHE_UPDATED_AT['host'],
            port=CONNECT_REDIS_CACHE_UPDATED_AT['port'],
            db=CONNECT_REDIS_CACHE_UPDATED_AT['db'])

        redis_client = redis.Redis(connection_pool=pool_redis)
        res = redis_client.get('updated_at.'+ str(key) +'.' + str(id))
        if res:
            res = parser.parse(res)
            res = parse_tz(res)
            return res
    except Exception as e:
        logging.exception(e)
    return None

def set_cache_updated_at(key, id, updated_at = datetime.datetime.now()):
    try:
        pool_redis = redis.ConnectionPool(
            host=CONNECT_REDIS_CACHE_UPDATED_AT['host'],
            port=CONNECT_REDIS_CACHE_UPDATED_AT['port'],
            db=CONNECT_REDIS_CACHE_UPDATED_AT['db'])

        redis_client = redis.Redis(connection_pool=pool_redis)
        redis_client.set('updated_at.'+ str(key) +'.' + str(id), str(updated_at))
        return True
    except Exception as e:
        logging.exception(e)
    return False

def get_cache_checksum(key, id, checksum):
    try:
        pool_redis = redis.ConnectionPool(
            host=CONNECT_REDIS_CACHE_CHECKSUM['host'],
            port=CONNECT_REDIS_CACHE_CHECKSUM['port'],
            db=CONNECT_REDIS_CACHE_CHECKSUM['db'])

        redis_client = redis.Redis(connection_pool=pool_redis)
        res = redis_client.get('checksum.'+ str(key) +'.'+ str(id) +'.'+ str(checksum))
        if res:
            return True
    except Exception as e:
        logging.exception(e)
    return False

def set_item_from_keys(item, data):
    for key in data:
        item.key = data[key]
    return True

def create_check_sum(keys):
    keys = json.dumps(
        keys,
        ensure_ascii=False)
    return zlib.crc32(keys.encode("utf-8"))

def set_cache_checksum(key, id, checksum):
    try:
        pool_redis = redis.ConnectionPool(
            host=CONNECT_REDIS_CACHE_CHECKSUM['host'],
            port=CONNECT_REDIS_CACHE_CHECKSUM['port'],
            db=CONNECT_REDIS_CACHE_CHECKSUM['db'])

        redis_client = redis.Redis(connection_pool=pool_redis)
        x = redis_client.keys('checksum.'+ str(key) +'.'+ str(id) +'.*')
        for old_key in x: redis_client.delete(old_key)
        redis_client.set('checksum.'+ str(key) +'.'+ str(id) +'.'+ str(checksum), 1)
        return True
    except Exception as e:
        logging.exception(e)
    return False
create_check_sum({'a':2})