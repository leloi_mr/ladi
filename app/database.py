import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

SQLALCHEMY_DATABASE_URL = 'mysql+pymysql://root:@127.0.0.1:3306/ladi'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    convert_unicode=True
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
db = scoped_session(SessionLocal)

Base = declarative_base()
Base.query = db.query_property()
Base.metadata.create_all(bind=engine)
