#!/usr/bin/env python
# -*- coding=utf-8 -*-
from flask import Flask, request, jsonify
from celery_main import *

app = Flask(__name__)

@app.route('/users/<user_id>/scan', methods=['POST'])
def catch_scan_by_user_id(user_id):
    try:
        res = push_command({
            'type': 'scan_user',
            'user_id': user_id
        })
        return jsonify(res), 200
        # hanlder_a_user().apply_async(args=(user_id,))
    except Exception as e:
        return jsonify(e), 500

@app.route('/shutdown_celery', methods=['GET'])
def shutdown_celery():
    try:
        res = shutdown()
        return jsonify(res), 200
    except Exception as e:
        return jsonify(e), 500

if __name__=="__main__":
    app.run(debug=True)