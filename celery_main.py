#!/usr/bin/env python
# -*- coding=utf-8 -*-
from concurrent.futures import ThreadPoolExecutor
from celery import Celery, bootsteps
from celery.bin import Option
import requests
import uuid, os
import hanging_threads
from app.database import *
from app.utils import *
from app.models.Account import Account
from app.models.AccountUser import AccountUser
from app.models.Admin import Admin
from app.models.Config import Config
from app.models.Group import Group
from app.models.Item import Item
from app.models.Ladi import Ladi
from app.models.LadiTag import LadiTag
from app.models.Tag import Tag
from app.models.User import User

STATUS_ACTIVE = 1
STATUS_INACTIVE = 2
STATUS_SCAN_ACTIVE = 1
STATUS_SCAN_INACTIVE = 2
STATUS_PASTE_ACTIVE = 1
STATUS_PASTE_INACTIVE = 2
STATUS_BLOCK_ACTIVE = 1
STATUS_BLOCK_INACTIVE = 2

## Start celery ##
# celery -A celery_main.celery worker -n self_killing -l info -P gevent

class RunController(bootsteps.Step):
    def __init__(self, worker, funtion, **options):
        print ('-------- Init Controller --------')
        run_controller.apply_async()

config = {}
config['broker_url'] = 'redis://localhost:6379/1'
config['task_serializer'] = 'json'
config['accept_content'] = ['json']
config['timezone'] = 'Asia/Ho_Chi_Minh'
config['task_acks_late'] = False
celery = Celery(__name__, broker=config['broker_url'])
celery.user_options['worker'].add(Option('--funtion', type=str, default='func'))
celery.steps['worker'].add(RunController)
celery.conf.update(config)

max_thread_pool_handler_commands = 1
max_thread_pool_handler_users = 5
max_thread_pool_handler_accounts = 5
max_thread_pool_handler_ladies = 20

CONNECT_REDIS = {
    'host': 'localhost',
    'port': 6379,
    'db': 4
}

@celery.task(name='run_controller')
def run_controller():
    # hanging_threads.run()

    try:
        while True:
            print('controller start')
            has_commands = has_commands_in_queue()
            if not has_commands:
                pass
            print('controller start thread pool')
            pool = ThreadPoolExecutor(
                max_workers=max_thread_pool_handler_commands)
            for i in range(max_thread_pool_handler_commands):
                command = pop_command()
                command = command and command[0] or None
                if command and command['type'] == 'scan_user':
                    user_id = command['user_id']
                    pool.submit(
                        hanlder_a_user,
                        user_id
                    )
                elif command and command['type'] == 'publish_ladi':
                    url = command['url']
                    token = command['token']
                    pool.submit(
                        publish_ladi,
                        url,
                        token
                    )
            print('controller end thread pool')
            pool.shutdown(wait=True)
            print('controller end')
            print ('---- sleeping (10) ----')
            # time.sleep(10)
            print ('---- sleep end ----')
            print ('-------------------')
            break
    except Exception as e:
        logging.exception(e)

@celery.task(name='hanlder_a_user')
def hanlder_a_user(user_id):
    print('hanlder_a_user start')
    try:
        user = User.query.get(user_id)
        if user and user.status == STATUS_ACTIVE and user.status_scan == STATUS_SCAN_ACTIVE:
            list_accounts = user.accounts
            if list_accounts:
                print('hanlder_a_user start thread pool')
                pool = ThreadPoolExecutor(
                    max_workers=max_thread_pool_handler_accounts)
                for account in list_accounts:
                    pool.submit(
                        hanlder_a_account,
                        account,
                        user
                    )
                    break
                print ('hanlder_a_user end thread pool')
                pool.shutdown(wait=True)
    except Exception as e:
        logging.exception(e)
    print('hanlder_a_user end')

@celery.task(name='hanlder_a_account')
def hanlder_a_account(account, user):
    print('hanlder_a_account start')
    try:
        if account and account.status == STATUS_ACTIVE and account.status_scan == STATUS_SCAN_ACTIVE:
            # if account.is_login():
            if True:
                # remote_ladies = account.get_all_remote_ladi()
                remote_ladies = {
                    "data": {
                        "total": 2886,
                        "limit": 10000,
                        "items": [
                            {
                                "scope_teams": [],
                                "scope_users": [],
                                "tags": [
                                    "5f0428743fe6127c1bd167f3"
                                ],
                                "_id": "5f053b27e09476169a272d83",
                                "store_id": "5b80bae280c18044b11eada8",
                                "owner_id": "5b80bae280c18044b11eada8",
                                "creator_id": "5b80bae280c18044b11eada8",
                                "name": "xuong",
                                "alias": "xuong",
                                "domain": "hoailam.hoanokhongmau.online",
                                "path": "xuong-ga",
                                "https": True,
                                "page_url": "https://hoailam.hoanokhongmau.online/xuong-ga",
                                "backup_count": 0,
                                "last_update_source": "2020-07-08T03:19:03.803Z",
                                "publish_platform": "LADIPAGEDNS",
                                "is_publish": True,
                                "is_delete": False,
                                "origin_id": "5f0428c14bb67a7c1daa1dc0",
                                "user_scopes": [],
                                "created_at": "2020-07-08T03:19:03.872Z",
                                "updated_at": "2020-07-08T03:20:00.429Z"
                            }
                        ]
                    }
                }
                if remote_ladies:
                    remote_ladies = remote_ladies['data']['items']
                    print('hanlder_a_account start thread pool')
                    pool = ThreadPoolExecutor(
                        max_workers=max_thread_pool_handler_ladies)
                    for remote_ladi in remote_ladies:
                        remote_id = remote_ladi['_id']

                        remote_updated_at = parser.parse(remote_ladi['updated_at'])
                        cache_updated_at = get_cache_updated_at(getattr(Ladi, '__tablename__'), remote_id)
                        if cache_updated_at:
                            if remote_updated_at < cache_updated_at:
                                # continue
                                pass
                            elif remote_updated_at - cache_updated_at < datetime.timedelta(minutes=15):
                                # continue
                                pass
                        local_ladi = Ladi.query.filter_by(remote_id=remote_id).first()
                        # if local_ladi:
                        if False:
                            local_ladi_updated_at = parse_tz(local_ladi.updated_at)
                            if cache_updated_at:
                                set_cache_updated_at(getattr(Ladi, '__tablename__'), local_ladi.remote_id, local_ladi_updated_at)
                            if remote_updated_at <= local_ladi_updated_at:
                                # continue
                                pass
                            elif remote_updated_at - local_ladi_updated_at < datetime.timedelta(minutes=15):
                                # continue
                                pass
                            if local_ladi.status == STATUS_ACTIVE and local_ladi.status_scan == STATUS_SCAN_ACTIVE :
                                handle_a_remote_ladi(remote_id, local_ladi, account)
                        else:
                            save_new_remote_ladi(remote_id, account, user)
                    print ('hanlder_a_account end thread pool')
                    pool.shutdown(wait=True)
    except Exception as e:
        logging.exception(e)
    print('hanlder_a_account end')

@celery.task(name='handle_a_remote_ladi')
def handle_a_remote_ladi(remote_id, local_ladi, account):
    print('handle_a_remote_ladi start')
    try:
        group = local_ladi.group
        if group and group.status == STATUS_ACTIVE and group.status_scan == STATUS_SCAN_ACTIVE:
            detail_remote_ladi = {
                "data": {
                    "source": "{\"title\":\"TRUNG TÂM SỨC KHỎE KHỚP XƯƠNG\",\"description\":\"TRUNG TÂM SỨC KHỎE KHỚP XƯƠNG\",\"keyword\":\"\",\"image\":\"\",\"favicon\":\"\",\"is_lazyload\":true,\"is_mobile_only\":true}\"",
                    "ladipage": {
                        "scope_teams": [],
                        "scope_users": [],
                        "tags": [
                            "5f0428743fe6127c1bd167f3"
                        ],
                        "_id": "5f053b27e09476169a272d83",
                        "store_id": "5b80bae280c18044b11eada8",
                        "owner_id": "5b80bae280c18044b11eada8",
                        "creator_id": "5b80bae280c18044b11eada8",
                        "name": "xuong",
                        "alias": "xuong",
                        "domain": "hoailam.hoanokhongmau.online",
                        "path": "xuong-ga",
                        "https": True,
                        "page_url": "https://hoailam.hoanokhongmau.online/xuong-ga",
                        "backup_count": 0,
                        "last_update_source": "2020-07-08T03:19:03.803Z",
                        "publish_platform": "LADIPAGEDNS",
                        "is_publish": True,
                        "is_delete": False,
                        "origin_id": "5f0428c14bb67a7c1daa1dc0",
                        "user_scopes": [],
                        "created_at": "2020-07-08T03:19:03.872Z",
                        "updated_at": "2020-07-08T03:20:00.429Z",
                        "subdomain": ""
                    },
                    "message": "Thành công",
                    "code": 200
                }
            }
            if detail_remote_ladi:
                remote_ladi = detail_remote_ladi['data']['ladipage']
                remote_ladi_source_dumps = detail_remote_ladi['data']['source']
                remote_ladi_tags = detail_remote_ladi['data']['ladipage']['tags']

                keys = local_ladi.keys_of_item(remote_ladi)
                if not local_ladi.match_checksum(keys):
                    local_ladi.save_checksum(keys)

                has_pasted = user.check_has_pasting(remote_ladi_source_dumps)
                #if (has_pasted and group_new.status_pasting == STATUS_PASTE_ACTIVE) or (not has_pasted and group_new.status_pasting == STATUS_PASTE_INACTIVE):
                if True:
                    set_cache_updated_at(getattr(Ladi, '__tablename__'), remote_ladi['_id'])
                    local_ladi.load_data_from_remote_json(remote_ladi)
                    local_ladi.create()
                else:
                    new_source = user.get_new_source(has_pasted, remote_ladi_source_dumps)
                    if new_source:
                        if account.update_remote_ladi(remote_id, new_source):
                            set_cache_updated_at(getattr(Ladi, '__tablename__'), remote_id)
                            push_command({
                                'type': 'publish_ladi',
                                'url': remote_id,
                                'token': account.token
                            })
                print ('handle_a_remote_ladi end thread pool')
    except Exception as e:
        logging.exception(e)
    print('handle_a_remote_ladi end')

@celery.task(name='save_new_remote_ladi')
def save_new_remote_ladi(remote_id, account, user):
    print('save_new_remote_ladi start')
    try:
        account_user = AccountUser.query.filter_by(account_id=account.id, user_id=user.id).first()
        group_new = account_user and Group.query.filter_by(account_user_id=account_user.id, type=getattr(Group, 'TYPE_NEW')).first() or None
        if group_new and group_new.status == STATUS_ACTIVE and group_new.status_scan == STATUS_SCAN_ACTIVE:
            # detail_remote_ladi = account.get_detail_remote_ladi(remote_id)
            detail_remote_ladi = {
                "data": {
                    "source": "{\"title\":\"TRUNG TÂM SỨC KHỎE KHỚP XƯƠNG\",\"description\":\"TRUNG TÂM SỨC KHỎE KHỚP XƯƠNG\",\"keyword\":\"\",\"image\":\"\",\"favicon\":\"\",\"is_lazyload\":true,\"is_mobile_only\":true}\"",
                    "ladipage": {
                        "scope_teams": [],
                        "scope_users": [],
                        "tags": [
                            "5f0428743fe6127c1bd167f3"
                        ],
                        "_id": "5f053b27e09476169a272d83",
                        "store_id": "5b80bae280c18044b11eada8",
                        "owner_id": "5b80bae280c18044b11eada8",
                        "creator_id": "5b80bae280c18044b11eada8",
                        "name": "xuong",
                        "alias": "xuong",
                        "domain": "hoailam.hoanokhongmau.online",
                        "path": "xuong-ga",
                        "https": True,
                        "page_url": "https://hoailam.hoanokhongmau.online/xuong-ga",
                        "backup_count": 0,
                        "last_update_source": "2020-07-08T03:19:03.803Z",
                        "publish_platform": "LADIPAGEDNS",
                        "is_publish": True,
                        "is_delete": False,
                        "origin_id": "5f0428c14bb67a7c1daa1dc0",
                        "user_scopes": [],
                        "created_at": "2020-07-08T03:19:03.872Z",
                        "updated_at": "2020-07-08T03:20:00.429Z",
                        "subdomain": ""
                    },
                    "message": "Thành công",
                    "code": 200
                }
            }
            if detail_remote_ladi:
                remote_ladi = detail_remote_ladi['data']['ladipage']
                remote_ladi_source_dumps = detail_remote_ladi['data']['source']
                remote_ladi_tags = detail_remote_ladi['data']['ladipage']['tags']

                set_cache_updated_at(getattr(Ladi, '__tablename__'), remote_id)
                local_ladi = Ladi()
                local_ladi.load_data_from_remote_json(remote_ladi)
                local_ladi.group_id = group_new.id
                keys = local_ladi.keys_of_item(remote_ladi)
                local_ladi.save_checksum(keys)
                # local_ladi.create()

                has_pasted = user.check_has_pasting(remote_ladi_source_dumps)
                #if (has_pasted and group_new.status_pasting == STATUS_PASTE_ACTIVE) or (not has_pasted and group_new.status_pasting == STATUS_PASTE_INACTIVE):
                if True:
                    new_source = user.get_new_source(has_pasted, remote_ladi_source_dumps)
                    print(new_source)
                    if new_source:
                        if account.update_remote_ladi(remote_id, new_source):
                            set_cache_updated_at(getattr(Ladi, '__tablename__'), remote_id)
                            push_command({
                                'type': 'publish_ladi',
                                'url': remote_id,
                                'token': account.token
                            })
                print('save_new_remote_ladi end thread pool')
    except Exception as e:
        logging.exception(e)
    print('save_new_remote_ladi end')

@celery.task(name='publish_ladi')
def publish_ladi(url, token):
    print('publishing')

@celery.task(name='has_commands_in_queue')
def has_commands_in_queue():
    try:
        pool_redis = redis.ConnectionPool(host=CONNECT_REDIS['host'],
                                          port=CONNECT_REDIS['port'],
                                          db=CONNECT_REDIS['db'])
        redis_client = redis.Redis(connection_pool=pool_redis)
        res = redis_client.llen('queue_commands')
        return res and True or False
    except Exception as e:
        logging.exception(e)
    return False

@celery.task(name='push_command')
def push_command(command):
    try:
        pool_redis = redis.ConnectionPool(host=CONNECT_REDIS['host'],
                                          port=CONNECT_REDIS['port'],
                                          db=CONNECT_REDIS['db'])
        redis_client = redis.Redis(connection_pool=pool_redis)

        # handle queue FULL
        count_retry = 0
        while True:
            length_queue = redis_client.llen('queue_commands')
            if length_queue < 10000:
                break
            print ('queue FULL (10000) ...')
            time.sleep(120)
            if count_retry > 5:
                return {
                    'error': {
                        'message': 'queue FULL',
                        'retry': True
                    }
                }
            count_retry += 1

        # handle the pushing
        redis_client.rpush('queue_commands', json.dumps(command, ensure_ascii=False))
        return True
    except Exception as e:
        logging.exception(e)
    return False

@celery.task(name='pop_command')
def pop_command(amount=1):
    try:
        pool_redis = redis.ConnectionPool(host=CONNECT_REDIS['host'],
                                          port=CONNECT_REDIS['port'],
                                          db=CONNECT_REDIS['db'])
        redis_client = redis.Redis(connection_pool=pool_redis)
        res = []
        count = 0
        while True:
            if count > amount:
                break
            command = redis_client.lpop('queue_commands')
            if command != None:
                command = json.loads(command)
                res.append(command)
                count += 1
                continue
            break
        return res
    except Exception as e:
        logging.exception(e)
    return None

if __name__ == '__main__':
    # os.system('celery -A celery_main.celery worker -n self_killing -l info -P gevent')
    # run_controller()
    user = User.query.get(1)
    account = Account.query.get(2)
    hanlder_a_account(account, user)